<?php
/**
 * Created by PhpStorm.
 * User: MONIR
 * Date: 12/25/2016
 * Time: 8:49 AM
 */
echo "First example, Array_chunk" . "<br>";
$input_array = array('a', 'b', 'c', 'd', 'e');
echo "<pre>";
//it's show 2 digit serial.
print_r(array_chunk($input_array, 2));
//it's show continue serial.
print_r(array_chunk($input_array, 3, true));



//Second example part
echo "Second example, Array_chunk" . "<br>";

function partition( $list, $p ) {
    $listlen = count( $list );
    $partlen = floor( $listlen / $p );
    $partrem = $listlen % $p;
    $partition = array();
    $mark = 0;
    for ($px = 0; $px < $p; $px++) {
        $incr = ($px < $partrem) ? $partlen + 1 : $partlen;
        $partition[$px] = array_slice( $list, $mark, $incr );
        $mark += $incr;
    }
    return $partition;
}

$citylist = array( "Chandler", "Flagstaff", "Gilbert", "Glendale", "Globe", "Mesa", "Miami",
    "Phoenix");
print_r( partition( $citylist, 3 ) );



//Third example part

echo "Third example, Aray_chunk" . "<br>" . "<br>";
$cars=array("Volvo","BMW","Toyota","Honda","Mercedes","Opel");
print_r(array_chunk($cars,2 ));
