<?php
/**
 * Created by PhpStorm.
 * User: MONIR
 * Date: 12/25/2016
 * Time: 9:37 AM
 */

$fname=array("Peter","Ben","Joe");
$age=array("35","37","43");

$c=array_combine($fname,$age);

echo "<pre>";
print_r($c);


//Second example part
echo "Second example, Array_chunk" . "<br>" . "<br>";

function array_combine_($keys, $values)
{
    $result = array();
    foreach ($keys as $i => $k) {
        $result[$k][] = $values[$i];
    }
    array_walk($result, create_function('&$v', '$v = (count($v) == 1)? array_pop($v): $v;'));
    return    $result;
}

print_r(array_combine_(Array('a','a','b'), Array(1,2,3)));