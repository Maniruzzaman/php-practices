<?php
$name = $email = $website = $comment = $gender = "";
$errname = $erremail = $errwebsite = $errgender = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (empty($_POST["name"])) {
        $errname = "Name is required.";
    } else {
        $name = $_POST["name"];
    }
    if (empty($_POST["email"])) {
        $erremail = "E-mail is required.";
    } else {
        $email = $_POST["email"];
    }
    if (empty($_POST["website"])) {
        $errwebsite = "Website is required.";
    } else {
        $website = $_POST["website"];
    }

    $comment = $_POST["comment"];

    if (empty($_POST["gender"])) {
        $errgender = "Gender is required.";
    } else {
        $gender = $_POST["gender"];
    }
}

echo "Name: " . $name . "<br>";
echo "E-mail: " . $email . "<br>";
echo "Website: " . $website . "<br>";
echo "Comment: " . $comment . "<br>";
echo "Gender: " . $gender;

?>
<html>
<body bgcolor="#8fbc8f">
<!--htmlspecialchars use for save to hacker your website-->
<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST">
    <table width="500">
        <tr>
            <td>Name: </td>
            <td><input type="text" name="name" />*<?php echo $errname; ?></td>
        </tr>
        <tr>
            <td>E-mail: </td>
            <td><input type="text" name="email" />*<?php echo $erremail; ?></td>
        </tr>
        <tr>
            <td>Website: </td>
            <td><input type="text" name="website" />*<?php echo $errwebsite; ?></td>
        </tr>
        <tr>
            <td>Comment: </td>
            <td><textarea name="comment" rows="5" cols="30"></textarea> </td>
        </tr>
        <tr>
            <td>Gender: </td>
            <td>
                <input type="radio" name="gender" value="Male" />Male
                <input type="radio" name="gender" value="Female" />Female
                *<?php echo $errgender; ?>
            </td>
        </tr>
        <tr>
            <td></td>
            <td><input type="submit" name="submit" value="Submit" /></td>
        </tr>
    </table>
</form>
</body>
</html>