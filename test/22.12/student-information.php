<?php
/**
 * Created by PhpStorm.
 * User: MONIR
 * Date: 12/22/2016
 * Time: 6:34 PM
 */

?>
<?php
if (isset($_POST['submit'])) {

    $fullname = $_POST['fullname'];
    $phone = $_POST['phone'];
    $email = $_POST['email'];
    $address = $_POST['address'];

    $gender = '';
    if (isset($_POST['gender'])) {
        $gender = $_POST['gender'];
    }
    $class = $_POST['class'];

    $subject = '';
    if (isset($_POST['subject'])) {
        $value = $_POST['subject'];
        foreach ($value as $val) {
            $subject .= $val . ', ';
        }
    }

    $password = $_POST['password'];
    $repassword = $_POST['repassword'];

    echo "Full Name: " . $fullname . "<br>";
    echo "Phone: " . $phone . "<br>";
    echo "E-mail: " . $email . "<br>";
    echo "Address: " . $address . "<br>";
    echo "Gender: " . $gender . "<br>";
    echo "Class: " . $class . "<br>";
    echo "Subject: " . $subject . "<br>";
    echo "Password: " . $password . "<br>";
    echo "Retype Password: " . $repassword . "<br>";


    // Validation Start
$error = array();
    if($fullname == ''){
        $error[] = 'Name field can not be empty.';
    }
    if(!preg_match("/^[A-Za-z]{3,}$/",$fullname)){
        $error[] = "Only Alphabet is allowed and minimum three character";
    }

    // Phone
    if (strlen($phone) != 11){
        $error[] = 'Invalid phone number';
    }

    //Email
    if(!preg_match("/^[A-Za-z]+[.]?[A-Za-z] * [@] [A-Za-z]{2, } [.] [A-Za-z] {2, }$/", $email)){
        $error[] = 'Invalid Email.';
    }

    //Password
    if(!preg_match("/^[*]{6, }$/", $password)){
        $error[] = 'Password must be minimum six character';
    }

    //Repassword
    if($password != $repassword){
        $error[] = 'Password does not match.';
    }

    //image/file
   // if (!empty($_FILES['gallary']['tmp_name'])) {
        $imagefiletype = pathinfo($_FILES['file']['name'],PATHINFO_EXTENSION );
        if ($imagefiletype != 'jpg' && $imagefiletype != 'png' && $imagefiletype != 'jpeg' && $imagefiletype != 'gif') {
            $error[] = 'Only JPG, JPEG, PNG or GIF image are allowed.';
        }
        if ($_FILES['file']['size'] > 500000) {
            $error[] = 'Sorry your file is too large.';
        }
 //   }

    //Error cheching
    if(empty($error)){

    }else{
        $html = '';
        foreach($error as $value){
            $html .= $value . "<br>";
        }
    }

    if(!empty($html)){
        echo "<div class='error'>".$html."</div>";
    }
}
?>




<html>
<head>
    <title>Student Information</title>

    <style>
        h1{
            text-align: center;
            color:blue;
        }
        table{
            width:500px;
            margin:auto;
            border:1px solid #ccc;
            padding:10px;
        }
        .required{
            color:red;
            
        }
        .error{
            text-align: center;
            color: #f00;
        }
    </style>
</head>
<body>
<h1>Student Information</h1>

<form action="" method="POST" enctype="multipart/form-data">
    <table width="400">
        <tr>
            <td>Full Name: </td>
            <td><input type="text" name="fullname" /> </td>
        </tr>
        <tr>
            <td>Phone: </td>
            <td><input type="text" name="phone" /> </td>
        </tr>
        <tr>
            <td>E-mail</td>
            <td><input type="text" name="email" /> </td>
        </tr>
        <tr>
            <td>Address: </td>
            <td><input type="text" name="address" /> </td>
        </tr>
        <tr>
            <td>Gender: </td>
            <td>
                <input type="radio" name="gender" value="Male" />Male
                <input type="radio" name="gender" value="Female" />Female
            </td>
        </tr>
        <tr>
            <td>Class: </td>
            <td>
                <select name="class">
                    <option selected>--Select--</option>
                    <option value="One">One</option>
                    <option value="Two">Tow</option>
                    <option value="Three">Three</option>
                    <option value="Four">Four</option>
                    <option value="Five">Five</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>Subject: </td>
            <td>
                <input type="checkbox" name="subject[]" value="Bangla" />Bangla
                <input type="checkbox" name="subject[]" value="English" />English
                <input type="checkbox" name="subject[]" value="Math" />Math
            </td>
        </tr>
        <tr>
            <td>Password: </td>
            <td><input type="password" name="password" /> </td>
        </tr>
        <tr>
            <td>Retype Password: </td>
            <td><input type="password" name="repassword" /> </td>
        </tr>
        <tr>
            <td>Files: </td>
            <td><input type="file" name="file" /> </td>
        </tr>
        <tr>
            <td></td>
            <td><input type="submit" name="submit" /> </td>
        </tr>
    </table>
</form>
</body>
</html>
