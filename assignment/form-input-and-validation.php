<?php
/**
 * Created by PhpStorm.
 * User: MONIR
 * Date: 12/23/2016
 * Time: 12:21 PM
 */
// Input part
if (isset($_POST["fullname"])){
    $name = $_POST["fullname"];
    echo "Name: " . $name . "<br>";
}
if (isset($_POST["email"])){
    $email = $_POST["email"];
    echo "E-mail: " . $email . "<br>";
}
if (isset($_POST["phone"])){
    $phone = $_POST["phone"];
    echo "Phone No: " . $phone . "<br>";
}
if (isset($_POST["gender"])){
    $gender = $_POST["gender"];
    echo "Gender: " . $gender . "<br>";
}

//Validation part
$errfullname = $erremail = $errphone = $errgender = "";
if ($_SERVER["REQUEST_METHOD"] == "POST"){
    if(empty($_POST["fullname"])){
        $errfullname = "<span class='error'> Name is required.</span>";
    }else {
        $fullname = $_POST["fullname"];
    }
    if(empty($_POST["email"])){
        $erremail = "<span class='error'> E-mail is required.</span>";
    }else {
        $email = $_POST["email"];
    }
    if(empty($_POST["phone"])){
        $errphone = "<span class='error'> Phone is required.</span>";
    }else{
        $phone = $_POST["phone"];
    }
    if (empty($_POST["gender"])){
        $errgender = "<span class='error'> Gender is requires.</span>";
    }else{
        $gender = $_POST["gender"];
    }
}

?>

<html>
<head>
    <style>
        .error {
            color:red;
        }
    </style>
</head>
<body>
<form action="" method="POST">
    <table>
        <tr>
            <td>Name: </td>
            <td><input type="text" name="fullname" /><span class="error">*</span><?php echo $errfullname; ?></td>
        </tr>
        <tr>
            <td>E-mail: </td>
            <td><input type="text" name="email" /><span class="error">*</span><?php echo $erremail; ?></td>
        </tr>
        <tr>
            <td>Phone No: </td>
            <td><input type="text" name="phone" /><span class="error">*</span><?php echo $errphone; ?></td>
        </tr>
        <tr>
            <td>Gender: </td>
            <td>
                <input type="radio" name="gender" value="Male" />Male
                <input type="radio" name="gender" value="Female" />Female
                <span class="error">*</span><?php echo $errgender; ?>
            </td>
        </tr>
        <tr>
            <td></td>
            <td><input type="submit" name="submit" /> </td>
        </tr>
    </table>
</form>
</body>
</html>
