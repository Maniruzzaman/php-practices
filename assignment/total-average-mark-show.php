<?php
/**
 * Created by PhpStorm.
 * User: MONIR
 * Date: 12/23/2016
 * Time: 4:23 PM
 */

//Input part
if (isset($_POST["studentname"])){
    $StudentName = $_POST["studentname"];
    echo "Student Name: " . $StudentName . "<br>";
}
if (isset($_POST["studentroll"])){
    $StudentRoll = $_POST["studentroll"];
    echo "Student Roll: " . $StudentRoll . "<br>";
}
//Total Mark part
if (isset($_POST["total"])) {
    $bangla = $_POST["bangla"];
    $english = $_POST["english"];
    $math = $_POST["math"];
    $biology = $_POST["biology"];

    $TotalMark = $bangla + $english + $math + $biology;
    echo "Total Mark: " . $TotalMark;

}

//Average Mark part
if (isset($_POST["average"])) {
    $bangla = $_POST["bangla"];
    $english = $_POST["english"];
    $math = $_POST["math"];
    $biology = $_POST["biology"];

    $TotalMark = $bangla + $english + $math + $biology;

    $subjectcount = array("bangla" , "english" , "math" , "biology");

    $average = count($subjectcount);

    echo "Average Mark: " . $TotalMark / $average;

}

//Validation part
$errstudentname = $errstudentroll = $errbangla= $errenglish = $errmath = $errbiology = "";


if ($_SERVER["REQUEST_METHOD"] == "POST"){
    if(empty($_POST["studentname"])){
        $errstudentname = "<span class=\"error\"> Name is required.</span>";
    }else{
        $studentname = $_POST["studentname"];
    }

    if(empty($_POST["studentroll"])){
        $errstudentroll = "<span class=\"error\"> Roll is required.</span>";
    }else{
        $studentroll = $_POST["studentroll"];
    }
    if(empty($_POST["bangla"])){
        $errbangla = "<span class=\"error\"> Mark is required.</span>";
    }else{
        $bangla = $_POST["bangla"];
    }
    if(empty($_POST["english"])){
        $errenglish = "<span class=\"error\"> Mark is required.</span>";
    }else{
        $english = $_POST["english"];
    }
    if(empty($_POST["math"])){
        $errmath = "<span class=\"error\"> Mark is required.</span>";
    }else{
        $math = $_POST["math"];
    }
    if(empty($_POST["biology"])){
        $errbiology = "<span class=\"error\"> Mark is required.</span>";
    }else{
        $biology = $_POST["biology"];
    }
}


?>

<html>
<head>
    <style>
        .error {
            color: red;
        }
    </style>
</head>
<body bgcolor="#8fbc8f">
<h1>Student Marksheet</h1>
<h3 class="error">* mark is required.</h3>

<form action="" method="POST">
    <table>
        <tr>
            <td>Student Name</td>
            <td>:</td>
            <td><input type="text" name="studentname" /><span class="error"> *</span><?php echo $errstudentname; ?> </td>
        </tr>
        <tr>
            <td>Student Roll No</td>
            <td>:</td>
            <td><input type="text" name="studentroll" /><span class="error"> *</span><?php echo $errstudentroll; ?></td>
        </tr>
        <tr>
            <td height="20px"></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>Bangla</td>
            <td>:</td>
            <td><input type="text" name="bangla" /><span class="error"> *</span> <?php echo $errbangla; ?></td>
        </tr>
        <tr>
            <td>English</td>
            <td>:</td>
            <td><input type="text" name="english" /><span class="error"> *</span> <?php echo $errenglish; ?></td>
        </tr>
        <tr>
            <td>Mathmatics</td>
            <td>:</td>
            <td><input type="text" name="math" /><span class="error"> *</span> <?php echo $errmath; ?></td>
        </tr>
        <tr>
            <td>Biology</td>
            <td>:</td>
            <td><input type="text" name="biology" /><span class="error"> *</span> <?php echo $errbiology; ?></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td>
                <input type="submit" name="total" value="Total Mark" />
                <input type="submit" name="average" value="Average Mark" />
                <input type="reset" name="reset" />
            </td>
        </tr>
    </table>
</form>
</body>
</html>