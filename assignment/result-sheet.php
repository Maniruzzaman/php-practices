<?php
/**
 * Created by PhpStorm.
 * User: MONIR
 * Date: 12/23/2016
 * Time: 8:28 PM
 */
/*A+ 94-10
A 90-93.99
A- 86-89.99

B+ 82-85.99
B  78-81.99
B- 74-77.99

Fail F 0-73.99
*/
/*
$mark = 85;

if($mark>=94){
    echo "Mark: A+";
}
if($mark>=90){
    echo "Mark: A";
}
if($mark>=86){
    echo "Mark: A-";
}
if($mark>=82){
    echo "Mark: B+";
}
if($mark>=78){
    echo "Mark: B";
}
if($mark>=78){
    echo "Mark: B-";
}
if($mark<78){
    echo "Mark: B";
}
*/

if(isset($_POST["mark"])) {
    $mark = $_POST["mark"];
    if ($mark >= 94 && $mark <= 100) {
        echo "Grade A+";
    }
    if ($mark >= 90 && $mark < 93.99) {
        echo "Grade A";
    }
    if ($mark >= 86 && $mark < 89.99) {
        echo "Grade A-";
    }
    if ($mark >= 82 && $mark < 85.99) {
        echo "Grade B+";
    }
    if ($mark >= 78 && $mark < 81.99) {
        echo "Grade B";
    }
    if ($mark >= 74 && $mark < 77.99) {
        echo "Grade B-";
    }
    if ($mark < 74) {
        echo "Grade F";
    }

}
?>

<html>
<head>

</head>
<body>
<form action="" method="POST">
    Give your Mark: <input type="text" name="mark" />
    <input type="submit" name="submit" />
</form>

<p>
A+ 94-10<br/>
A 90-93.99<br/>
A- 86-89.99<br/>

B+ 82-85.99<br/>
B  78-81.99<br/>
B- 74-77.99<br/>

Fail F 0-73.99<br/>
</p>
</body>
</html>
