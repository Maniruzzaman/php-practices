<?php
$name = $email = $website = $comment = $gender = "";
$errname = $erremail = $errweb = $errgender = "";


if ($_SERVER["REQUEST_METHOD"] == "POST"){

    if (empty($_POST["name"])){
        $errname = "<span style='color:red'> Name is required.</span>";
    }else {
        $name = validate($_POST["name"]);
    }
    if (empty($_POST["email"])){
        $erremail = "<span style='color:red'> E-mail is required.</span>";
    }else {
        $email = validate($_POST["email"]);
    }
    if (empty($_POST["website"])){
        $errweb = "<span style='color:red'> Website is required.</span>";
    }else {
        $website = validate($_POST["website"]);
    }
    if (empty($_POST["gender"])){
        $errgender = "<span style='color:red'> Gender is required.</span>";
    }else {
        $gender = validate($_POST["gender"]);
    }
    $comment = validate($_POST["comment"]);

    echo "Name: " . $name . "<br>";
    echo "E-mail: " . $email . "<br>";
    echo "Website: " . $website . "<br>";
    echo "Gender: " . $gender . "<br>";
    echo "Comment: " . $comment . "<br>";
}

function validate($data){
    return $data;
}
?>



<html>
<head>
    <title>Form Page</title>
</head>
<body>
<form action="" method="POST">
    <table border="0px" width="400px">

        <p style="color:red;">* required field</p>

        <tr>
            <td>Name: </td>
            <td><input type="text" name="name"> *<?php echo $errname ?></td>
        </tr>
        <tr>
            <td>E-mail: </td>
            <td><input type="text" name="email"> *<?php echo $erremail ?></td>
        </tr>
        <tr>
            <td>Website: </td>
            <td><input type="text" name="website"> *<?php echo $errweb ?></td>
        </tr>
        <tr>
            <td>Comment: </td>
            <td><textarea name="comment" rows="5" cols="50" ></textarea> </td>
        </tr>
        <tr>
            <td>Gender: </td>
            <td>
                <input type="radio" name="gender" value="female" />Female
                <input type="radio" name="gender" value="male" />Male
                 *<?php echo $errgender ?>
            </td>
        </tr>
        <tr>
            <td></td>
            <td><input type="submit" name="submit" value="Submit" /> </td>
        </tr>
    </table>
</form>
</body>
</html>