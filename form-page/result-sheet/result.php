<?php
$ban = $eng = $mat = $che = "";
$errbangla = $errenglish = $errmathmatich = $errchemistry = "";


if ($_SERVER["REQUEST_METHOD"] == "POST"){

    if (empty($_POST["ban"])){
        $errbangla = "<span style='color:red'> Mark is required.</span>";
    }else {
        $ban = validate($_POST["ban"]);
    }
    if (empty($_POST["eng"])){
        $errenglish = "<span style='color:red'> Mark is required.</span>";
    }else {
        $eng = validate($_POST["eng"]);
    }
    if (empty($_POST["mat"])){
        $errmathmatich = "<span style='color:red'> Mark is required.</span>";
    }else {
        $mat = validate($_POST["mat"]);
    }
    if (empty($_POST["che"])){
        $errchemistry = "<span style='color:red'> Mark is required.</span>";
    }else {
        $che = validate($_POST["che"]);
    }


}

function validate($data){
    return $data;
}

if (isset($_POST["total"])){
$Bangla = $_POST["ban"];
$English = $_POST["eng"];
$Math = $_POST["mat"];
$Cheamistry = $_POST["che"];
$Total = $Bangla + $English + $Math + $Cheamistry;

echo "Total Mark: " . $Total . "<br>" . "<br>" . "<br>";
}

if (isset($_POST["average"])){
    $Bangla = $_POST["ban"];
    $English = $_POST["eng"];
    $Math = $_POST["mat"];
    $Cheamistry = $_POST["che"];
    $Total = $Bangla + $English + $Math + $Cheamistry;

    $avg = array("$Bangla", "$English", "$Math", "$Cheamistry");
    $length = count($avg);
    echo "Average Mark: " . $Total / $length . "<br>" . "<br>" . "<br>";

}



?>
<html>
<head>
    <title>Result Sheet</title>
</head>
<body>
<form action="" method="POST">
    <table border="0px" width="400px">
        <tr>
            <td>Bangla: </td>
            <td><input type="number" name="ban">  *<?php echo $errbangla ?></td>
        </tr>
        <tr>
            <td>English: </td>
            <td><input type="number" name="eng">  *<?php echo $errenglish ?></td>
        </tr>
        <tr>
            <td>Mathmatich: </td>
            <td><input type="number" name="mat">  *<?php echo $errmathmatich ?></td>
        </tr>
        <tr>
            <td>Cheamistry: </td>
            <td><input type="number" name="che">  *<?php echo $errchemistry ?></td>
        </tr>
        <tr>
            <td></td>
            <td>
                <input type="submit" name="total" value="Total Number">
                <input type="submit" name="average" value="Average Number">

            </td>
        </tr>
    </table>
</form>
</body>
</html>